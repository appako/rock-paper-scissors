# rock-paper-scissors game

This is a repository with game rock-paper-scissors in browser (Typescript, React)

# Build

```sh
npm install
gulp bundle
```

The above task creates `dist/bundle.js`.

# Run

```sh
cd dist
npm install -g http-server
http-server -p 4000
```

And go to `localhost:4000` in your browser.
