/// <reference path="../typings/bundle.d.ts" />

import * as React from 'react';


const rock = 'камень', paper = 'бумага', scissors = 'ножницы';
const win = "Вы выиграли!", loss = "Вы проиграли!", draw = "Ничья!";
type Choice = 'камень'|'бумага'|'ножницы';

interface IChoice {
    user: Choice
    pc: Choice
}

export class Main extends React.Component<{}, {}> {
    state: {
        user: string
        pc: string
        result: string
        firstGame: boolean
        userCounter: number
        pcCounter: number
        play: boolean
    }
    constructor () {
        super();
    }

    componentWillMount(){
        this.setState({firstGame: true, userCounter: 0, pcCounter: 0});
    }

    game(subject: Choice){
        if (!this.state.play) this.setState({play: true});
        else return;
        let arr = ['камень', 'ножницы', 'бумага', 1, 2, 3 ];
        let i = 0;
        this.setState({user: subject, result: arr[i]})
        setTimeout(()=>{
            let subjectUser = subject;
            switch (subjectUser) {
                case rock:
                case paper:
                case scissors: {
                        let userCounter = this.state.userCounter, pcCounter = this.state.pcCounter;
                        let subjectComputer = this.computerChoice();
                        let result = this.compare({user: subjectUser, pc: subjectComputer});
                        if(result === win) userCounter++;
                        else if(result === loss) pcCounter++;
                        this.setState({
                            user: subjectUser,
                            pc: subjectComputer,
                            result: result,
                            userCounter: userCounter,
                            pcCounter: pcCounter
                        });
                    }
                    break;
                default:
                    break;
            }
            this.setState({play: false})
            clearInterval(interval);
        }, 3000);

        let interval = setInterval(()=>{
            i++;
            this.setState({result: arr[i]});
        }, 500);
    }
    compare(choice: IChoice){
        if (choice.user === choice.pc){
            return draw;
        } else if (choice.user === rock) {
            if (choice.pc === scissors) return win;
            else if (choice.pc === paper) return loss;
        } else if (choice.user === paper) {
            if (choice.pc === rock) return win;
            else if (choice.pc === scissors) return loss;
        } else if (choice.user === scissors) {
            if (choice.pc === rock) return loss;
            else if (choice.pc === paper) return win;
        }
    }

    computerChoice(): Choice {
        var randomValue = Math.random();
        if (randomValue <= 0.5) return scissors;
        else if(randomValue <= 0.75) return paper;
        else return rock;
    }
    displayBorder(subject: Choice){
        if(this.state.user === subject) return '2px solid gray';
        else return '2px solid white';
    }

    render () {
        return (
        <div style={{textAlign: 'center'}}>
            <h1>Игра "Камень-ножницы-бумага"</h1>
            <h2>Выбери картинку</h2>
            <div>
                <image src='/img/rock.jpeg'
                    onClick={()=>{this.game(rock)}}
                    width='100'
                    style={{marginRight: '50px', border: this.displayBorder(rock)}}
                />
                <image src='/img/scissors.jpeg'
                    onClick={()=>{this.game(scissors)}}
                    width='100'
                    style={{marginRight: '50px', border: this.displayBorder(scissors)}}
                />
                <image src='/img/paper.jpeg'
                    onClick={()=>{this.game(paper)}}
                    width='100'
                    style={{border: this.displayBorder(paper)}}
                    />
            </div>
            <p style={{color: 'red', fontSize: '50px'}}>{this.state.result && this.state.result}</p>
            <table style={{width: "100%", marginTop: 50, fontSize: '20px'}}>
                <thead>
                    <tr>
                        <th>Вы выбрали</th>
                        <th>Счет</th>
                        <th>ПК выбрал</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style={{width: '100px'}}>{this.state.user && this.state.user}</td>
                        <td style={{width: '100px'}}>{this.state.userCounter + ' : ' + this.state.pcCounter }</td>
                        <td style={{width: '100px'}}>{this.state.pc && this.state.pc}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        );
    }
}
//<button onClick={()=>{this.game()}}>{this.state.firstGame ? 'Играть' : 'Еще раз'}</button>
